# Pear Admin Layui     :fa-meh-o: 

最近更新  2020 年 4 月 4 日  
 

#### 软件介绍

致力于提供 简洁 而 不简单 的前端框架

基于Layui的后台管理系统模板，扩展Layui原生UI样式，整合第三方开源组件，提供便捷快速的开发方式，延续LayuiAdmin

的设计风格，持续完善的样式与组件的维护，基于异步Ajax的菜单构建，相对完善的多标签页，单标签页的共存，为使用者提

供相对完善的开发方案，只为成为更好的轮子，项目不定时更新，建议 Star watch 一份

#### 共享地址

开源地址:https://gitee.com/Jmysy/Pear-One-Layui

Pear Admin v 1.0 :http://jmysy.gitee.io/pear-one-layui/Pear%20Admin%20v%201.0/

Pear Admin v 2.0 :http://jmysy.gitee.io/pear-one-layui/Pear%20Admin%20v%202.0/

交流链接: [交流群](https://jq.qq.com/?_wv=1027&k=5OdSmve)

Pear Admin Layui 目前支持单页面与多标签页的切换，仅仅通过简单的API接口初始化就可以, 遇到问题及时拍板，加群讨论，如有意愿加入开源的
伙伴可群内私信作者，第三期更新订版时会推出相关API文档


![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/161541_614d4797_4835367.png "JOBEZCP@%WOC4}B7RKEVQRO.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/161551_61854cd5_4835367.png "K_[4YON[[TXT{6RGAEGGV3E.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/161610_af1360fb_4835367.png "N0E}6VD1HH0C4UETP07QSF5.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0217/161626_c20fab08_4835367.png "_RH)_TZ$BNINQ}9081FF54V.png")


### 优秀开源推荐

SpringBoot 后台管理系统：[SpringBoot_v2](http://gitee.com/bdj/SpringBoot_v2)


本框架为开源框架，Apache 开源协议，支持商用，学习

如果对你有帮助 欢迎 Start

项 目 不 间 断 更 新 ......


